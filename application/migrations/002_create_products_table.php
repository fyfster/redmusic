<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_products_table extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'bar_code' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'bar_code_opt' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'description' => array(
                                'type' => 'TEXT',

                                'null' => TRUE,
                        ),
                        'price' => array(
                            'type' => 'DECIMAL',
                            'constraint' => '10,2',
                        ),
                        'price_with_vat' => array(
                            'type' => 'DECIMAL',
                            'constraint' => '10,2',
                        ),
                        'vat' => array(
                            'type' => 'DECIMAL',
                            'constraint' => '10,2',
                        ),
                        'image' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'null' => TRUE,
                        ),
                        'category_id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                        ),

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_key('category_id');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (category_id) REFERENCES categories(id)');
                $this->dbforge->create_table('products');
        }

        public function down()
        {
                $this->dbforge->drop_table('products');
        }
}
