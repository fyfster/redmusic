<html>
<style>
input[type=text], select, textarea {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.label-message {
    width: 100px;
}

.label-status-danger {
    background-color: red;
}

.label-status-success {
    background-color: green;
}

</style>
<body>
    <head>
        <!-- <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet" type="text/css" /> -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title><?php $title ?></title>
    </head>
    <body>
        <?php if (isset($message)) {?>
            <div class="alert alert-<?php echo $message['type']; ?>" role="alert">
                 <?php echo $message['body']; ?>
            </div>
        <?php }?>
