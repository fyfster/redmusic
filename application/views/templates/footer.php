<em>&copy; 2020 Visean Razvan</em>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
$( ".delete-button" ).on( "click", function() {
    if (confirm("Sigur stergeti informatia") == true) {
        $.ajax({
          url: $(this).data('url')
      }).done(function(response) {
          if (response.type == 'success') {
              location.reload();
          } else {
              $('.alert-ajax').remove();
              $('body').prepend('<div class="alert alert-ajax alert-'+ response.type +'" role="alert">'+ response.body +'</div>')
          }
        });
    }
});
// $( ".button-search-categories" ).on( "click", function() {
//     var form = $( ".search-categories" );
//     var action = form.prop("action");
//     if (form.find('input').val() != '') {
//         form.prop( "action", action + "/" +form.find('input').val());
//         form.submit();
//     }
// });
</script>

</html>
