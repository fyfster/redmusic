<div class="row content-box">
    <div class="panel panel-primar col-lg-12">
        <div class="panel-heading">
            <h2 class="float-left">Categorii conform pretului</h2>
            <a href="<?php echo base_url("list-categories"); ?>" type="button" class="btn btn-primary float-right">Lista categori</a>
        </div>
        <div class="panel-body">
            <form class="form-inline" method="GET" action="<?php echo base_url("price-comparison"); ?>" style="margin-top:10px">
                <div class="form-group mb-2">
                    <label>Pret: </label>
                    <input type="number" class="form-control" name="price" value="<?php echo (isset($price) ? $price : "");?>" >
                </div>
                <div class="form-group mb-2">
                    <label>Nr minim produse: </label>
                    <input type="number" class="form-control" name="min_nr" value="<?php echo (isset($min_nr) ? $min_nr : "");?>" >
                </div>
              <button type="submit" class="btn btn-primary mb-2">Cauta</button>
              <a style="margin-left: 10px;" href="<?php echo base_url("price-comparison"); ?>" class="btn btn-info mb-2">Reset</a>
            </form>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nume</th>
                  <th scope="col">Descriere</th>
                  <th scope="col">Nr. Produse</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($categories as $category) {?>
                      <tr>
                          <td><?php echo $category['name']?></td>
                          <td><?php echo $category['description']?></td>
                          <td><?php echo $category['nrProducts']?></td>
                      </tr>
                  <?php } ?>
              </tbody>
            </table>
        </div>
    </div>

    </div>
</div>
