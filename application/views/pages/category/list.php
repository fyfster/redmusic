<div class="row content-box">
    <div class="panel panel-primar col-lg-12">
        <div class="panel-heading">
            <h2 class="float-left">Lista categorii</h2>
            <a href="<?php echo base_url("create-category"); ?>" type="button" class="btn btn-success float-right">Adauga Categorie</a>
            <a href="<?php echo base_url("price-comparison"); ?>" type="button" style="margin-right: 10px;" class="btn btn-info float-right">Comparator Pret</a>
        </div>
        <div class="panel-body">
            <form class="form-inline" method="GET" action="<?php echo base_url("list-categories"); ?>" style="margin-top:10px">
              <div class="form-group mb-2">
                <label>Nume: </label>
                <input type="text" class="form-control" name="name" value="<?php echo (isset($name) ? $name : "");?>" >
            </div>
            <div class="form-group mb-2" style="width: 100%;">
              <label>Ordonare dupa: </label>
              <select type="text" class="form-control" name="order_col" >
                  <option value=""></option>
                  <option value="name" <?php echo ((!empty($order_col) &&  $order_col == "name") ? " selected" : "");?> >Nume</option>
                  <option value="description"<?php echo ((!empty($order_col) &&  $order_col == "description") ? " selected" : "");?>>Descriere</option>
                  <option value="nrProducts"<?php echo ((!empty($order_col) &&  $order_col == "nrProducts") ? " selected" : "");?>>Nr. Produse</option>
              </select>
              <select type="text" class="form-control" name="order_direction" >
                  <option value=""></option>
                  <option value="asc"<?php echo ((!empty($order_direction) &&  $order_direction == "asc") ? " selected" : "");?>>ASC</option>
                  <option value="desc"<?php echo ((!empty($order_direction) &&  $order_direction == "desc") ? " selected" : "");?>>DESC</option>
              </select>
            </div>
              <button type="submit" class="btn btn-primary mb-2">Cauta</button>
              <a style="margin-left: 10px;" href="<?php echo base_url("list-categories"); ?>" class="btn btn-info mb-2">Reset</a>
            </form>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nume</th>
                  <th scope="col">Descriere</th>
                  <th scope="col">Nr. Produse</th>
                  <th scope="col">Actiuni</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($categories as $category) {?>
                      <tr>
                          <td><?php echo $category['name']?></td>
                          <td><?php echo $category['description']?></td>
                          <td><?php echo $category['nrProducts']?></td>
                          <td>
                                <a href="<?php echo base_url("list-products/" . $category['id']); ?>" type="button" class="btn btn-secondary">Lista Produse</a>
                                <a href="<?php echo base_url("edit-category/" . $category['id']); ?>" type="button" class="btn btn-primary">Editeaza</a>
                                <a href="javascript:;" data-url="<?php echo base_url("delete-category/" . $category['id']); ?>" type="button" class="btn btn-danger delete-button">Sterge</a>
                          </td>
                      </tr>
                  <?php } ?>
              </tbody>
            </table>
        </div>
    </div>

    </div>
</div>
