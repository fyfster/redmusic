<form  method="Post" action="<?php echo (isset($isEdit) ? base_url("edit-category/" . $id) : base_url("create-category")); ?>">
  <div class="form-group">
    <label for="name">Nume</label>
    <input type="text"  name="name" value="<?php echo (isset($name) ? $name : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="description">Descriere</label>
    <textarea class="form-control" name="description" rows="3"><?php echo (isset($description) ? $description : "");?></textarea>
  </div>
   <button type="submit" class="btn btn-primary">Submit</button>
   <a href="<?php echo base_url("list-categories"); ?>" class="btn btn-success">Lista categori</a>
</form>
