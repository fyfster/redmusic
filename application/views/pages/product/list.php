<div class="row content-box">
    <div class="panel panel-primar col-lg-12">
        <div class="panel-heading">
            <h3 class="float-left">Lista produse pentru: <u><?php echo $categoryName?></u></h3><br><br>
            <h4>Nr produse: <?php echo $productCount?></h4>
            <a href="<?php echo base_url("create-product/" . $categoryId); ?>" type="button" class="btn btn-success float-right">Adauga Produs</a>
            <a href="<?php echo base_url("list-categories"); ?>" type="button" style="margin-right: 10px;" class="btn btn-primary float-right">Lista categori</a>
        </div>
        <div class="panel-body">
            <form class="form-inline" method="GET" action="<?php echo base_url("list-products/" . $categoryId); ?>" style="margin-top:10px">
              <div class="form-group mb-2">
                <label>Nume: </label>
                <input type="text" class="form-control" name="name" value="<?php echo (isset($name) ? $name : "");?>" >
              </div>
              <div class="form-group mb-2">
                <label>Pret achizitie: </label>
                <input type="number" class="form-control" name="price" value="<?php echo (isset($price) ? $price : "");?>" >
              </div>
              <div class="form-group mb-2">
                <label>Pret vanzare (cu tva): </label>
                <input type="number" class="form-control" name="price_with_vat" value="<?php echo (isset($price_with_vat) ? $price_with_vat : "");?>" >
              </div>
              <div class="form-group mb-2">
                <label>Cod de bare: </label>
                <input type="text" class="form-control" name="bar_code" value="<?php echo (isset($bar_code) ? $bar_code : "");?>" >
              </div>
              <div class="form-group mb-2">
                <label>Cod de bare secundar: </label>
                <input type="text" class="form-control" name="bar_code_opt" value="<?php echo (isset($bar_code_opt) ? $bar_code_opt : "");?>" >
              </div>

              <div class="form-group mb-2" style="width: 100%;">
                <label>Ordonare dupa: </label>
                <select type="text" class="form-control" name="order_col" >
                    <option value=""></option>
                    <option value="name" <?php echo ((!empty($order_col) &&  $order_col == "name") ? " selected" : "");?> >Nume</option>
                    <option value="description"<?php echo ((!empty($order_col) &&  $order_col == "description") ? " selected" : "");?>>Descriere</option>
                    <option value="price" <?php echo ((!empty($order_col) &&  $order_col == "price") ? " selected" : "");?> >Pret achizitie</option>
                    <option value="price_with_vat" <?php echo ((!empty($order_col) &&  $order_col == "price_with_vat") ? " selected" : "");?> >Pret vanzare (cu tva)</option>
                    <option value="vat" <?php echo ((!empty($order_col) &&  $order_col == "vat") ? " selected" : "");?> >TVA</option>
                    <option value="bar_code" <?php echo ((!empty($order_col) &&  $order_col == "bar_code") ? " selected" : "");?> >Cod de bare</option>
                    <option value="bar_code_opt" <?php echo ((!empty($order_col) &&  $order_col == "bar_code_opt") ? " selected" : "");?> >Cod de bare secundar</option>
                </select>
                <select type="text" class="form-control" name="order_direction" >
                    <option value=""></option>
                    <option value="asc"<?php echo ((!empty($order_direction) &&  $order_direction == "asc") ? " selected" : "");?>>ASC</option>
                    <option value="desc"<?php echo ((!empty($order_direction) &&  $order_direction == "desc") ? " selected" : "");?>>DESC</option>
                </select>
              </div>

              <button type="submit" class="btn btn-primary mb-2">Cauta</button>
              <a style="margin-left: 10px;" href="<?php echo base_url("list-products/" . $categoryId); ?>" class="btn btn-info mb-2">Reset</a>
            </form>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nume</th>
                  <th scope="col">Descriere</th>
                  <th scope="col">Pret achizitie</th>
                  <th scope="col">Pret vanzare (cu tva)</th>
                  <th scope="col">TVA</th>
                  <th scope="col">Cod de bare</th>
                  <th scope="col">Cod de bare secundar</th>
                  <th scope="col">Imagine</th>
                  <th scope="col">Actiuni</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($products as $product) {?>
                      <tr>
                          <td><?php echo $product['name']?></td>
                          <td><?php echo $product['description']?></td>
                          <td><?php echo $product['price']?></td>
                          <td><?php echo $product['price_with_vat']?></td>
                          <td><?php echo $product['vat']?></td>
                          <td><?php echo $product['bar_code']?></td>
                          <td><?php echo $product['bar_code_opt']?></td>
                          <td><?php echo $product['image']?> <?php if(!empty($product['image'])){?> <img style="height:100px; width:100px;" src="<?php echo base_url("uploads/" . $product['image']); ?>"><?php }?></td>
                          <td>
                                <a href="<?php echo base_url("edit-product/" . $product['id']); ?>" type="button" class="btn btn-primary">Editeaza</a>
                                <a href="javascript:;" data-url="<?php echo base_url("delete-product/" . $product['id']); ?>" type="button" class="btn btn-danger delete-button">Sterge</a>
                          </td>
                      </tr>
                  <?php } ?>
              </tbody>
            </table>
        </div>
    </div>

    </div>
</div>



<div class="row content-box">

</div>
