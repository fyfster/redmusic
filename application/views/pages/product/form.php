<form  method="Post" action="<?php echo (isset($isEdit) ? base_url("edit-product/" . $id) : base_url("create-product/" . $categoryId)); ?>" enctype='multipart/form-data'>
  <div class="form-group">
    <label for="name">Nume</label>
    <input type="text"  name="name" value="<?php echo (isset($name) ? $name : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="description">Descriere</label>
    <textarea class="form-control" name="description" rows="3"><?php echo (isset($description) ? $description : "");?></textarea>
  </div>
  <div class="form-group">
    <label for="price">Pretul achizitiei </label>
    <input type="number"  name="price" value="<?php echo (isset($price) ? $price : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="price_with_vat">Pret vanzare (cu tva)</label>
    <input type="number"  name="price_with_vat" value="<?php echo (isset($price_with_vat) ? $price_with_vat : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="vat">TVA (%)</label>
    <input type="number"  name="vat" value="<?php echo (isset($vat) ? $vat : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="bar_code">Codul de bare</label>
    <input type="text"  name="bar_code" value="<?php echo (isset($bar_code) ? $bar_code : "");?>" class="form-control">
  </div>
  <div class="form-group">
    <label for="bar_code_opt">Codul de bare secundar</label>
    <input type="text"  name="bar_code_opt" value="<?php echo (isset($bar_code_opt) ? $bar_code_opt : "");?>" class="form-control">
  </div>
  <?php if (!empty($image)) {?>
      <div class="form-group">
        <label for="bar_code_opt">Imaginea curenta</label>
        <img src="<?php echo base_url("uploads/" . $image); ?>">
      </div>
  <?php }?>
  <div class="form-group">
    <label for="image">Imagine</label>
    <input type="file"  name="image" class="form-control">
  </div>
   <button type="submit" class="btn btn-primary">Submit</button>
   <a href="<?php echo base_url("list-products/" . $categoryId); ?>" class="btn btn-success">Lista produse</a>
</form>
