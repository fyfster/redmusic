<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Model {
    CONST BASE_TABLE_NAME = 'products';
    private $id;
    private $name;
    private $description;
    private $price;
    private $priceWithVat;
    private $vat;
    private $barCode;
    private $barCodeOpt;
    private $image;
    private $categoryId;

    /*
     * Getters
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceWithVat()
    {
        return $this->priceWithVat;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function getBarCode()
    {
        return $this->barCode;
    }

    public function getBarCodeOpt()
    {
        return $this->barCodeOpt;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /*
     * Setters
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setPriceWithVat($priceWithVat)
    {
        $this->priceWithVat = $priceWithVat;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;
    }

    public function setBarCodeOpt($barCodeOpt)
    {
        $this->barCodeOpt = $barCodeOpt;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    public function save()
    {
        $data = array(
                'name' => $this->getName(),
                'description' => $this->getDescription(),
                'price' => $this->getPrice(),
                'price_with_vat' => $this->getPriceWithVat(),
                'bar_code' => $this->getBarCode(),
                'bar_code_opt' => $this->getBarCodeOpt(),
                'image' => $this->getImage(),
                'category_id' => $this->getCategoryId(),
                'vat' => $this->getVat()
        );

        $this->db->insert(self::BASE_TABLE_NAME, $data);
    }

    public function update()
    {
        $data = array(
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'price' => $this->getPrice(),
            'price_with_vat' => $this->getPriceWithVat(),
            'bar_code' => $this->getBarCode(),
            'bar_code_opt' => $this->getBarCodeOpt(),
            'image' => $this->getImage(),
            'category_id' => $this->getCategoryId(),
            'vat' => $this->getVat()
        );
        $this->db->where('id', $this->getId());
        $this->db->update(self::BASE_TABLE_NAME, $data);
    }

    public function remove()
    {
        $this->db->where('id', $this->getId());
        $this->db->delete(self::BASE_TABLE_NAME);
    }

    public function loadBy($field, $value)
    {
        $query = $this->db->get_where(self::BASE_TABLE_NAME, array($field => $value), 1, 0);
        $row = $query->row();
        if (isset($row)) {
            $this->id = $row->id;
            $this->name = $row->name;
            $this->description = $row->description;
            $this->price = $row->price;
            $this->priceWithVat = $row->price_with_vat;
            $this->barCode = $row->bar_code;
            $this->barCodeOpt = $row->bar_code_opt;
            $this->image = $row->image;
            $this->categoryId = $row->category_id;
            $this->vat = $row->vat;
        } else {
            throw new \Exception("Error loading product with field" . $field . " and value " . $value, 1);
        }
    }

    public function checkValueExists($field, $value)
    {
        $query = $this->db->get_where(self::BASE_TABLE_NAME, array($field => $value), 1, 0);
        $row = $query->row();
        return isset($row) ? $row->id : 0;
    }

    public function getProductsWithFilter($searchFilter, $orderBy = null, $order = null)
    {
        $query = $this->db->get(self::BASE_TABLE_NAME);
        $this->db->select('*');
        $this->db->from(self::BASE_TABLE_NAME . ' as p');

        if (!empty($searchFilter['categoryId'])) {
            $this->db->where('category_id', $searchFilter['categoryId']);
        }
        if (!empty($searchFilter['name'])) {
            $this->db->like('name', $searchFilter['name']);
        }
        if (!empty($searchFilter['price'])) {
            $this->db->where('price', $searchFilter['price']);
        }
        if (!empty($searchFilter['price_with_vat'])) {
            $this->db->where('price_with_vat', $searchFilter['price_with_vat']);
        }
        if (!empty($searchFilter['bar_code'])) {
            $this->db->where('bar_code', $searchFilter['bar_code']);
        }
        if (!empty($searchFilter['bar_code_opt'])) {
            $this->db->where('bar_code_opt', $searchFilter['bar_code_opt']);
        }

        if ($orderBy != null){
            $this->db->order_by($orderBy, $order);
        }

        $query = $this->db->get();

        return $this->returnArrayFromColumns($query);
    }

    public function getProductsCountWithFilter($searchFilter)
    {
        $query = $this->db->get(self::BASE_TABLE_NAME);
        $this->db->from(self::BASE_TABLE_NAME . ' as p');

        if (!empty($searchFilter['categoryId'])) {
            $this->db->where('category_id', $searchFilter['categoryId']);
        }
        if (!empty($searchFilter['name'])) {
            $this->db->like('name', $searchFilter['name']);
        }
        if (!empty($searchFilter['price'])) {
            $this->db->where('price', $searchFilter['price']);
        }
        if (!empty($searchFilter['price_with_vat'])) {
            $this->db->where('price_with_vat', $searchFilter['price_with_vat']);
        }
        if (!empty($searchFilter['bar_code'])) {
            $this->db->where('bar_code', $searchFilter['bar_code']);
        }
        if (!empty($searchFilter['bar_code_opt'])) {
            $this->db->where('bar_code_opt', $searchFilter['bar_code_opt']);
        }

        $query = $this->db->count_all_results();

        return $query;
    }
}
