<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Model {
    CONST BASE_TABLE_NAME = 'categories';
    CONST PRODUCTS_TABLE_NAME = 'products';
    private $id;
    private $name;
    private $description;

    /*
     * Getters
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /*
     * Setters
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function getAllCategories()
    {
        $query = $this->db->get(self::BASE_TABLE_NAME);
        return $this->returnArrayFromColumns($query);
    }

    public function getCategoriesWithFilter($searchFilter, $orderBy = null, $order = null)
    {
        $query = $this->db->get(self::BASE_TABLE_NAME);
        $this->db->select('c.*, COUNT(p.id) as nrProducts');
        $this->db->from(self::BASE_TABLE_NAME . ' as c');
        $this->db->join('products as p', 'c.id = p.category_id', 'left');
        $this->db->group_by("c.id");


        if (!empty($searchFilter['name'])) {
            $this->db->like('c.name', $searchFilter['name']);
        }

        if (!empty($searchFilter['price'])) {
            $this->db->where('p.price > ', $searchFilter['price']);
        }

        if (!empty($searchFilter['min_nr'])) {
            $this->db->HAVING('nrProducts >= ', $searchFilter['min_nr']);
        }

        if ($orderBy != null){
            $this->db->order_by($orderBy, $order);
        }

        $query = $this->db->get();

        return $this->returnArrayFromColumns($query);
    }

    public function save()
    {
        $data = array(
                'name' => $this->getName(),
                'description' => $this->getDescription()
        );

        $this->db->insert(self::BASE_TABLE_NAME, $data);
        $this->id = $this->db->insert_id();
        return true;
    }

    public function update()
    {
        $data = array(
                'name' => $this->getName(),
                'description' => $this->getDescription()
        );
        $this->db->where('id', $this->getId());
        $this->db->update(self::BASE_TABLE_NAME, $data);
        return true;
    }

    public function remove()
    {
        $this->db->trans_start();

        $this->db->where('category_id', $this->getId());
        $this->db->delete(self::PRODUCTS_TABLE_NAME);

        $this->db->where('id', $this->getId());
        $this->db->delete(self::BASE_TABLE_NAME);

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function loadBy($field, $value)
    {
        $query = $this->db->get_where(self::BASE_TABLE_NAME, array($field => $value), 1, 0);
        $row = $query->row();
        if (isset($row)) {
            $this->id = $row->id;
            $this->name = $row->name;
            $this->description = $row->description;
        } else {
            throw new \Exception("Error loading category with field" . $field . " and value " . $value, 1);
        }
    }
}
