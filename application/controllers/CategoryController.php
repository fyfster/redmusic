<?php

class CategoryController extends MY_Controller{

    public function listPriceComparisons()
    {
        $data = [
            "price" => !empty($this->input->get("price")) ? $this->input->get("price") : "",
            "min_nr" => !empty($this->input->get("min_nr")) ? $this->input->get("min_nr") : "",
        ];
        $searchFilter = [
            'price' => $this->input->get("price"),
            'min_nr' => $this->input->get("min_nr"),
        ];
        $this->load->model('category');
        $data['categories'] = $this->category->getCategoriesWithFilter($searchFilter);
        $this->returnView($data, 'category\price');
    }

    public function listCategories()
    {
        $data = [
            "name" => !empty($this->input->get("name")) ? $this->input->get("name") : "",
            "order_col" => !empty($this->input->get("order_col")) ? $this->input->get("order_col") : "",
            "order_direction" => !empty($this->input->get("order_direction")) ? $this->input->get("order_direction") : "",
        ];
        $searchFilter = [];
        if(!empty($this->input->get("name"))) {
            $searchFilter = ['name' => $this->input->get("name")];
        }
        $orderByColumn = empty($this->input->get("order_col")) ? "c.id" : $this->input->get("order_col");
        $orderDirection = empty($this->input->get("order_direction")) ? "asc" : $this->input->get("order_direction");
        $this->load->model('category');
        $data['categories'] = $this->category->getCategoriesWithFilter($searchFilter, $orderByColumn, $orderDirection);
        $this->returnView($data, 'category\list');
    }

    public function createCategory()
    {
        $data = [];
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['message'] = $this->categoryCreate();
            if ($data['message']['type'] == self::RESPONSE_DANGER) {
                $data['name'] = $this->input->post('name');
                $data['description'] = $this->input->post('description');
            }
        }

        $this->returnView($data, 'category\form');
    }

    /**
     * Create category using post information
     *
     * @return array
     */
    private function categoryCreate()
    {
        if (empty($this->input->post('name'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Numele lipseste din cerere.');
        }

        $this->load->model('category');
        $this->category->setName($this->input->post('name'));
        $this->category->setDescription($this->input->post('description'));

        try {
            $this->category->save();
        } catch (\Exception $e) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la crearea categoriei');
        }
        return array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes');
    }

    public function editCategory($id)
    {
        $data = [
            'isEdit' => 1,
            'id' => $id
        ];
        $this->load->model('category');
        try {
            $this->category->loadBy("id", $id);
        } catch (\Exception $e) {
            $this->load->helper('url');
            redirect('list-categories');
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['message'] = $this->categoryEdit();
        }

        $data['name'] = !empty($this->input->post('name')) ? $this->input->post('name') : $this->category->getName();
        $data['description'] = !empty($this->input->post('description')) ? $this->input->post('description') : $this->category->getDescription();

        $this->returnView($data, 'category\form');
    }

    /**
     * Update category using post information
     *
     * @return array
     */
    private function categoryEdit()
    {
        if (empty($this->input->post('name'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Numele lipseste din cerere.');
        }

        $this->category->setName($this->input->post('name'));
        $this->category->setDescription($this->input->post('description'));

        try {
            $this->category->update();
        } catch (\Exception $e) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la actualizarea categoriei');
        }
        return array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes');
    }

    public function deleteCategory($id)
    {
        $data = [];
        $this->load->model('category');
        try {
            $this->category->loadBy("id", $id);
        } catch (\Exception $e) {
            return $this->returnJson(array('type' => self::RESPONSE_DANGER, 'body' => $e->getMessage()));
        }

        if ($this->category->remove() == false) {
            return $this->returnJson(array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la stergerea categoriei'));
        }

        return $this->returnJson(array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes'));
    }
}
