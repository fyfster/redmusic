<?php

class ProductController extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('category');
    }

    public function listProducts($categoryId)
    {
        $data = [
            "name" => !empty($this->input->get("name")) ? $this->input->get("name") : "",
            "price" => !empty($this->input->get("price")) ? $this->input->get("price") : "",
            "price_with_vat" => !empty($this->input->get("price_with_vat")) ? $this->input->get("price_with_vat") : "",
            "bar_code" => !empty($this->input->get("bar_code")) ? $this->input->get("bar_code") : "",
            "bar_code_opt" => !empty($this->input->get("bar_code_opt")) ? $this->input->get("bar_code_opt") : "",
            "order_col" => !empty($this->input->get("order_col")) ? $this->input->get("order_col") : "",
            "order_direction" => !empty($this->input->get("order_direction")) ? $this->input->get("order_direction") : "",
        ];
        try {
            $this->category->loadBy("id", $categoryId);
        } catch (\Exception $e) {
            $this->load->helper('url');
            redirect('list-categories');
        }
        $searchFilter = $this->getProductFilters();
        $searchFilter["categoryId"] = $categoryId;
        $orderByColumn = empty($this->input->get("order_col")) ? "id" : $this->input->get("order_col");
        $orderDirection = empty($this->input->get("order_direction")) ? "asc" : $this->input->get("order_direction");
        $this->load->model('product');

        $data['categoryName'] = $this->category->getName();
        $data['categoryId'] = $this->category->getId();
        $data['products'] = $this->product->getProductsWithFilter($searchFilter, $orderByColumn, $orderDirection);
        $data['productCount'] = $this->product->getProductsCountWithFilter($searchFilter);
        $this->returnView($data, 'product\list');
    }

    private function getProductFilters()
    {
        $searchFilter = [];
        $searchFilter["name"] = $this->input->get("name");
        $searchFilter["price"] = $this->input->get("price");
        $searchFilter["price_with_vat"] = $this->input->get("price_with_vat");
        $searchFilter["bar_code"] = $this->input->get("bar_code");
        $searchFilter["bar_code_opt"] = $this->input->get("bar_code_opt");
        return $searchFilter;
    }

    public function createProduct($categoryId)
    {
        $data = [];
        try {
            $this->category->loadBy("id", $categoryId);
            $data['categoryId'] = $this->category->getId();
        } catch (\Exception $e) {
            $this->load->helper('url');
            redirect('list-categories');
        }
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->model('product');
            $data['message'] = $this->productCreate();
            if ($data['message']['type'] == self::RESPONSE_DANGER) {
                $data['name'] = $this->input->post('name');
                $data['description'] = $this->input->post('description');
                $data['price'] = $this->input->post('price');
                $data['price_with_vat'] = $this->input->post('price_with_vat');
                $data['bar_code'] = $this->input->post('bar_code');
                $data['bar_code_opt'] = $this->input->post('bar_code_opt');
                $data['vat'] = $this->input->post('vat');
            }
        }

        $this->returnView($data, 'product\form');
    }

    /**
     * Create product using post information
     *
     * @return array
     */
    private function productCreate()
    {
        $generalProductValidation = $this->generalProductValidation();
        if (!empty($generalProductValidation)) {
            return $generalProductValidation;
        }

        if ($this->product->checkValueExists("name", $this->input->post('name')) != 0) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Numele produsului deja exista');
        }

        if ($this->product->checkValueExists("bar_code", $this->input->post('bar_code')) != 0) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Codul de bare deja exista');
        }

        if ($this->product->checkValueExists("bar_code_opt", $this->input->post('bar_code_opt')) != 0) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Codul de bare secundar deja exista');
        }

        $this->setProductInfo();
        $this->product->setCategoryId($this->category->getId());

        if (isset($_FILES) && !empty($_FILES['image']['name'])) {
            $uploadImage = $this->uploadImage();
            if (!empty($uploadImage)) {
                return $uploadImage;
            }
        }

        try {
            $this->product->save();
        } catch (\Exception $e) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la crearea produsului');
        }
        return array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes');
    }

    /**
     * Set base product properties values
     *
     * @return void
     */
    private function setProductInfo()
    {
        $this->product->setName($this->input->post('name'));
        $this->product->setDescription($this->input->post('description'));
        $this->product->setPrice($this->input->post('price'));
        $this->product->setPriceWithVat($this->input->post('price_with_vat'));
        $this->product->setVat($this->input->post('vat'));
        $this->product->setBarCode($this->input->post('bar_code'));
        $this->product->setBarCodeOpt($this->input->post('bar_code_opt'));
    }

    /**
     * General validation for product post items
     *
     * @return array
     */
    private function generalProductValidation()
    {
        if (empty($this->input->post('name'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Numele lipseste din cerere.');
        }

        if (empty($this->input->post('price'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Pretul achizitiei lipseste din cerere.');
        }
        if (!is_numeric($this->input->post('price'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Pretul achizitiei nu este numar.');
        }

        if (empty($this->input->post('price_with_vat'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Pret vanzare (cu tva) lipseste din cerere.');
        }
        if (!is_numeric($this->input->post('price_with_vat'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Pret vanzare (cu tva) nu este numar.');
        }

        if (empty($this->input->post('vat'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'TVA lipseste din cerere.');
        }
        if (!is_numeric($this->input->post('vat'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'TVA nu este numar.');
        }

        if (empty($this->input->post('bar_code'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Codul de bare lipseste din cerere.');
        }

        if (empty($this->input->post('bar_code_opt'))) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Codul de bare secundar lipseste din cerere.');
        }

        return array();
    }

    public function editProduct($id)
    {
        $data = [
            'isEdit' => 1,
            'id' => $id
        ];
        $this->load->model('product');
        try {
            $this->product->loadBy("id", $id);
            $data['categoryId'] = $this->product->getCategoryId();
        } catch (\Exception $e) {
            $this->load->helper('url');
            redirect('list-categories');
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['message'] = $this->productEdit();
        }

        $data['name'] = !empty($this->input->post('name')) ? $this->input->post('name') : $this->product->getName();
        $data['description'] = !empty($this->input->post('description')) ? $this->input->post('description') : $this->product->getDescription();
        $data['price'] =!empty($this->input->post('price')) ? $this->input->post('price') : $this->product->getPrice();
        $data['price_with_vat'] = !empty($this->input->post('price_with_vat')) ? $this->input->post('price_with_vat') : $this->product->getPriceWithVat();
        $data['bar_code'] = !empty($this->input->post('bar_code')) ? $this->input->post('bar_code') : $this->product->getBarCode();
        $data['bar_code_opt'] = !empty($this->input->post('bar_code_opt')) ? $this->input->post('bar_code_opt') : $this->product->getBarCodeOpt();
        $data['vat'] = !empty($this->input->post('vat')) ? $this->input->post('vat') : $this->product->getVat();
        $data['image'] = $this->product->getImage();

        $this->returnView($data, 'product\form');
    }

    /**
     * Update product using post information
     *
     * @return array
     */
    private function productEdit()
    {
        $generalProductValidation = $this->generalProductValidation();
        if (!empty($generalProductValidation)) {
            return $generalProductValidation;
        }

        $this->setProductInfo();

        try {
            $this->product->update();
        } catch (\Exception $e) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la actualizarea produsului');
        }
        return array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes');
    }

    public function deleteProduct($id)
    {
        $data = [];
        $this->load->model('product');
        try {
            $this->product->loadBy("id", $id);
        } catch (\Exception $e) {
            return $this->returnJson(array('type' => self::RESPONSE_DANGER, 'body' => $e->getMessage()));
        }

        if ($this->product->getImage() != null) {
            $this->load->helper("file");
            unlink(FCPATH . "/uploads/" . $this->product->getImage());
        }
        try {
            $this->product->remove();
        } catch (\Exception $e) {
            return array('type' => self::RESPONSE_DANGER, 'body' => 'Eroare la stergerea produsului');
        }

        return $this->returnJson(array('type' => self::RESPONSE_SUCCESS, 'body' => 'Succes'));

    }

    /**
     * Generates config array for file upload
     *
     * @return array
     */
    private function getUploadConfigValues()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = true;
        $config['max_size'] = 10000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        return $config;
    }

    /**
     * Uploads an image given in a multidata form
     *
     * @return array
     */
    private function uploadImage()
    {
        $config = $this->getUploadConfigValues();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            return array('type' => self::RESPONSE_DANGER, 'body' => $this->upload->display_errors());
        }
        $uploadData = $this->upload->data();
        $this->product->setImage($uploadData['file_name']);
        return array();
    }
}
