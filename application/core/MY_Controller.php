<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    CONST RESPONSE_DANGER = 'danger';
    CONST RESPONSE_SUCCESS = 'success';
    public function returnView($data, $page)
    {
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')) {
            show_404();
        }
        $data['title'] = ucfirst($page);

        $this->load->helper('url');
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer');
    }

    public function returnJson($data)
    {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
}
