<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        /**
         * Converts the result of a query into an array with arrays
         *
         * @param CI_DB_mysqli_result $result result of a query
         * @param bool $isOneDimensional result given is one dimensional
         * @return array
         */
        protected function returnArrayFromColumns($result, $isOneDimensional = false)
        {
            $return = [];
            if (empty($result)) {
                return $return;
            }
            foreach ($result->result() as $row) {
                $return[] = json_decode(json_encode($row), true);
            }

            return $return;
        }
}
