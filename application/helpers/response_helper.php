<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function returnView($data, $page)
{
    var_dump("dsadsa");
    if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')) {
        show_404();
    }
    $data['title'] = ucfirst($page);

    $this->load->view('templates/header', $data);
    $this->load->view('pages/'.$page, $data);
    $this->load->view('templates/footer');
}
